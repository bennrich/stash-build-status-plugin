package it.com.bolyuba.stash.plugin;

import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.webdriver.stash.StashTestedProduct;
import com.atlassian.webdriver.stash.element.BuildStatusDialog;
import com.atlassian.webdriver.stash.element.ProjectRepositoryTable;
import com.atlassian.webdriver.stash.element.ProjectTable;
import com.atlassian.webdriver.stash.page.FileBrowserPage;
import com.atlassian.webdriver.stash.page.PullRequestOverviewPage;
import com.atlassian.webdriver.stash.page.StashHomePage;
import com.atlassian.webdriver.stash.page.StashLoginPage;
import com.google.common.io.Files;
import it.com.bolyuba.stash.plugin.helpers.ForkRepoPage;
import it.com.bolyuba.stash.plugin.helpers.MyFileBrowserPage;
import it.com.bolyuba.stash.plugin.helpers.UserRepositoryPage;
import it.com.bolyuba.stash.plugin.helpers.UserRepositoryPullRequestCreatePage;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.eclipse.jgit.api.*;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Iterator;

/**
 * @author Georgy Bolyuba (georgy@bolyuba.com)
 */
public class ReportStatusTest {

    protected static final StashTestedProduct STASH = TestedProductFactory.create(StashTestedProduct.class);
    public static final String USER_NAME = "admin";
    public static final String PASSWORD = "admin";
    public static final String HOSTNAME = "georgy-home";
    public static final int PORT = 7990;
    public static final String SCHEME = "http";


    /**
     * Very simple test to check that we replicating the build status. Note: you will need to enable replication in the rep_1
     * manually before running this test
     *
     * @throws IOException
     * @throws GitAPIException
     */
    @Test
    public void testReportStatus_OK() throws IOException, GitAPIException {

        StashHomePage stashHomePage = STASH.visit(StashLoginPage.class).loginAsSysAdmin(StashHomePage.class);
        ProjectTable.ProjectRow projectByName = stashHomePage.getProjectTable().findProjectByName("Project 1");

        String projectKey = projectByName.getProjectKey();
        ProjectRepositoryTable.ProjectRepositoryRow rep1Row = projectByName.goToProjectPage().getRepositoryTable().findRepositoryByName("rep_1");
        String repositorySlug = rep1Row.getRepositorySlug();

        FileBrowserPage rep1Page = rep1Row.goToRepositoryPage();

        String targetHttpCloneUrl = STASH.getTester().getDriver().findElement(By.id("clone-repo-button")).getAttribute("href");

        MyFileBrowserPage myFileBrowserPage = STASH.visit(MyFileBrowserPage.class, projectKey, repositorySlug);

        ForkRepoPage forkRepoPage = myFileBrowserPage.clickForkButton();

        UserRepositoryPage userRepositoryPage = forkRepoPage.doForkAsAdmin();

        cloneChangeCommitAndPush(userRepositoryPage.getHttpCloneUrl(), "admin", "admin");

        UserRepositoryPullRequestCreatePage userRepositoryPullRequestCreatePage = userRepositoryPage.clickCreatePullRequest();

        userRepositoryPullRequestCreatePage.getSourceBranchSelector().open().selectItemByName("master");

        userRepositoryPullRequestCreatePage.submit();

        // Let Stash do automerge magic
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ignore) {
        }

        String automergeSHA1 = getAutomergeSHA1(targetHttpCloneUrl, "admin", "admin");

        Assert.assertNotNull("Cannot find automerge ID", automergeSHA1);

        DefaultHttpClient httpClient = new DefaultHttpClient();
        httpClient.getCredentialsProvider().setCredentials(new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT),
                new UsernamePasswordCredentials(USER_NAME, PASSWORD));

        HttpHost targetHost = new HttpHost(HOSTNAME, PORT, SCHEME);

        // Create AuthCache instance
        AuthCache authCache = new BasicAuthCache();
        // Generate BASIC scheme object and add it to the local auth cache
        BasicScheme basicAuth = new BasicScheme();
        authCache.put(targetHost, basicAuth);

        // Add AuthCache to the execution context
        BasicHttpContext localcontext = new BasicHttpContext();
        localcontext.setAttribute(ClientContext.AUTH_CACHE, authCache);

        HttpPost request = new HttpPost(SCHEME +"://" + HOSTNAME + ":" + PORT + "/stash/rest/build-status/1.0/commits/" + automergeSHA1);
        StringEntity params = new StringEntity("{" +
                "\"state\": \"SUCCESSFUL\"," +
                "\"key\": \"test-build-key\"," +
                "\"name\": \"test-build-name\"," +
                "\"url\": \"http://test-build-url\"," +
                "\"description\": \"test-build-description\"" +
                "}");
        request.addHeader("content-type", "application/json");
        request.setEntity(params);
        HttpResponse response = httpClient.execute(targetHost, request, localcontext);

        Assert.assertEquals("Something wrong with http post request", 204, response.getStatusLine().getStatusCode());


        PullRequestOverviewPage pullRequestOverviewPage = STASH.visit(PullRequestOverviewPage.class, projectKey, repositorySlug, 1);

        pullRequestOverviewPage.hasBuildStatus();
        BuildStatusDialog buildStatusDialog = pullRequestOverviewPage.clickBuildStatus();
        BuildStatusDialog.BuildStatusLine line = buildStatusDialog.getBuildStatusLines().iterator().next();

        Assert.assertEquals("Status is wrong", "Successful", line.getStatus());
    }


    private void cloneChangeCommitAndPush(@Nonnull String httpCloneUrl, String username, String pass) throws IOException, GitAPIException {
        File tempDir = Files.createTempDir();
        tempDir.deleteOnExit();
        String repoPath = tempDir.getAbsolutePath();
        File repoDir = new File(repoPath);

        FileRepositoryBuilder builder = new FileRepositoryBuilder();
        Repository repository = builder.setGitDir(repoDir).readEnvironment().build();

        Git git = new Git(repository);

        CloneCommand clone = git.cloneRepository();
        clone.setBare(false);
        clone.setCloneAllBranches(true);
        clone.setDirectory(repoDir).setURI(httpCloneUrl);

        UsernamePasswordCredentialsProvider credentialsProvider = new UsernamePasswordCredentialsProvider(username, pass);

        clone.setCredentialsProvider(credentialsProvider);
        git = clone.call();

        File file = new File(repoPath + File.separator + "test.txt");
        try {
            file.delete();
            Files.append("Some changes in the target file\n", file, Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }

        AddCommand add = git.add();
        add.addFilepattern("test.txt").call();

        CommitCommand commit = git.commit();
        commit.setMessage("Test pull request build status").call();

        PushCommand push = git.push();
        push.setCredentialsProvider(credentialsProvider).setPushAll().call();
    }


    private String getAutomergeSHA1(@Nonnull String httpCloneUrl, String username, String pass) throws GitAPIException, IOException {
        File tempDir = Files.createTempDir();
        tempDir.deleteOnExit();
        String repoPath = tempDir.getAbsolutePath();
        File repoDir = new File(repoPath);

        FileRepositoryBuilder builder = new FileRepositoryBuilder();
        Repository repository = builder.setGitDir(repoDir).readEnvironment().build();

        Git git = new Git(repository);

        CloneCommand clone = git.cloneRepository();
        clone.setBare(false);
        clone.setCloneAllBranches(true);
        clone.setDirectory(repoDir).setURI(httpCloneUrl);

        UsernamePasswordCredentialsProvider credentialsProvider = new UsernamePasswordCredentialsProvider(username, pass);

        clone.setCredentialsProvider(credentialsProvider);
        git = clone.call();

        Iterator<Ref> iterator = git.lsRemote().setCredentialsProvider(credentialsProvider).call().iterator();

        while (iterator.hasNext()) {
            Ref ref = iterator.next();
            if (ref.getName().equalsIgnoreCase("refs/pull-requests/1/merge")) {
                return ref.getObjectId().getName();
            }
        }
        return null;
    }

}