package it.com.bolyuba.stash.plugin.helpers;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.webdriver.stash.page.FileBrowserPage;

/**
 * @author Georgy Bolyuba (georgy@bolyuba.com)
 */
public class ForkRepoPage extends FileBrowserPage {

    // Should probably extend BaseRepositoryPage

    @ElementBy(id = "fork-repo-submit")
    private PageElement forkButton;

    public ForkRepoPage(String projectKey, String repoSlug) {
        super(projectKey, repoSlug);
    }

    @Override
    public String getUrl() {
        return super.getUrl().replace("/browse", "?fork");
    }

    // Do not know a better way to do this, userSlug is required
    public UserRepositoryPage doForkAs(String userSlug) {
        waitForPageLoad(new Runnable() {
            @Override
            public void run() {
                forkButton.click();
            }
        });

        return pageBinder.bind(UserRepositoryPage.class, userSlug, this.slug);
    }

    public UserRepositoryPage doForkAsAdmin() {
        return doForkAs("admin");
    }
}
