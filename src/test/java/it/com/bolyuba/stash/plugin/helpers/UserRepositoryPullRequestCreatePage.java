package it.com.bolyuba.stash.plugin.helpers;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.webdriver.stash.page.PullRequestCreatePage;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author Georgy Bolyuba (georgy@bolyuba.com)
 */
public class UserRepositoryPullRequestCreatePage extends PullRequestCreatePage {

    protected String userSlug;

    protected String repoSlug;

    @ElementBy(id = "submit-form")
    private PageElement creatPullRequestButton;

    @ElementBy(id = "sourceBranch")
    private PageElement sourceBranchSelectorButton;

    public UserRepositoryPullRequestCreatePage(String userSlug, String repoSlug) {
        super(userSlug, repoSlug);
        this.userSlug = userSlug;
        this.repoSlug = repoSlug;
    }

    @Override
    public String getUrl() {
        return String.format("/users/%s/repos/%s/pull-requests?create", userSlug, repoSlug);
    }

    public void submit() {
        creatPullRequestButton.click();
    }

    public void openSourceBranchSelector() {
        int attemptsLeft = 3;
        sourceBranchSelectorButton.click();

        while (attemptsLeft > 0) {
            try {
                WebDriverWait wait = new WebDriverWait(this.driver, 5);
                wait.until(ExpectedConditions.elementToBeClickable(By.id("sourceBranchDialog")));
                break;
            } catch (TimeoutException e) {
                sourceBranchSelectorButton.click();
                --attemptsLeft;
            }
        }
    }
}