package com.bolyuba.stash.plugin;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.stash.build.BuildStatus;
import com.atlassian.stash.build.BuildStatusService;
import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.exception.NoSuchChangesetException;
import com.atlassian.stash.pull.PullRequest;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.scm.*;
import com.atlassian.stash.util.Page;
import com.atlassian.utils.process.ProcessException;
import com.atlassian.utils.process.Watchdog;
import com.bolyuba.stash.plugin.config.db.StatusSettings;
import net.java.ao.Query;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author Georgy Bolyuba (georgy@bolyuba.com)
 */
public class AutomergeServiceImpl implements AutomergeService {

    private final ScmService scmService;
    private final BuildStatusService buildStatusService;
    private final ActiveObjects ao;

    public AutomergeServiceImpl(ScmService scmService, BuildStatusService buildStatusService, ActiveObjects ao) {
        this.scmService = scmService;
        this.buildStatusService = buildStatusService;
        this.ao = ao;
    }

    @Override
    public Changeset findAutomergeChangeset(PullRequest pullRequest) {
        Repository toRefRepo = pullRequest.getToRef().getRepository();
        ScmCommandBuilder<?> builder = scmService.createBuilder(toRefRepo);

        // get sha1 from that "special" branch
        String potentialAutomergeSha1 = builder.command("rev-parse")
                .argument("refs/pull-requests/" + pullRequest.getId() + "/merge").build(new CommandOutputHandler<String>() {
                    private String result;

                    @Nullable
                    @Override
                    public String getOutput() {
                        String temp = result;
                        result = null;
                        return temp;
                    }

                    @Override
                    public void process(InputStream inputStream) throws ProcessException {
                        try {
                            result = new BufferedReader(new InputStreamReader(inputStream)).readLine();
                        } catch (IOException e) {
                            throw new ProcessException(e);
                        }
                    }

                    @Override
                    public void complete() throws ProcessException {
                    }

                    @Override
                    public void setWatchdog(Watchdog watchdog) {
                    }
                }).call();


        return findChangeset(potentialAutomergeSha1, toRefRepo);
    }


    private Changeset findChangeset(String sha1, Repository repository) {
        ScmCommandFactory factory = scmService.getCommandFactory(repository);
        try {
            return factory.commit(new CommitCommandParameters.Builder().changesetId(sha1).build()).call();
        } catch (NoSuchChangesetException ignore) {
        }
        return null;
    }

    public BuildStatus findLastStatus(String updatedSHA1) {
        Page<? extends BuildStatus> all = buildStatusService.findAll(updatedSHA1);
        Iterator<? extends BuildStatus> iterator = all.getValues().iterator();

        if (!iterator.hasNext()) {
            return null;
        }

        BuildStatus result = iterator.next();

        while (iterator.hasNext()) {
            BuildStatus next = iterator.next();
            if (next.getDateAdded().after(result.getDateAdded())) {
                result = next;
            }
        }

        return result;
    }

    @Override
    public boolean shouldReplicate(@Nonnull Integer repoId) {
        return getStorage(repoId).getReplicate();
    }

    @Override
    public void setReplicate(@Nonnull final Integer repoId, final boolean enabled) {
        ao.executeInTransaction(new TransactionCallback<Object>() {
            @Override
            public Object doInTransaction() {
                StatusSettings storage = getStorage(repoId);
                storage.setReplicate(enabled);
                storage.save();
                return null;
            }
        });
    }

    @Override
    public boolean shouldCheckMerge(@Nonnull Integer repoId) {
        return getStorage(repoId).getMergeCheck();
    }

    @Override
    public void setCheckMerge(@Nonnull final Integer repoId, final boolean enabled) {
        ao.executeInTransaction(new TransactionCallback<Object>() {
            @Override
            public Object doInTransaction() {
                StatusSettings storage = getStorage(repoId);
                storage.setMergeCheck(enabled);
                storage.save();
                return null;
            }
        });
    }

    private StatusSettings getStorage(int repoId) {
        StatusSettings[] repoAutomergeBuildStatusSettingses = ao.find(StatusSettings.class,
                Query.select().where("REPO_ID = ? ORDER BY ID", repoId));

        if ((repoAutomergeBuildStatusSettingses == null) || (repoAutomergeBuildStatusSettingses.length == 0)) {
            StatusSettings settings = ao.create(StatusSettings.class);
            settings.setReplicate(false);
            settings.setRepoId(repoId);
            // no need to save it, just return
            return settings;
        }

        // return the last one (not that we expect there to be more than one, but still)
        return repoAutomergeBuildStatusSettingses[repoAutomergeBuildStatusSettingses.length - 1];
    }
}
