package com.bolyuba.stash.plugin;

import com.atlassian.stash.build.BuildStatus;
import com.atlassian.stash.build.BuildStatusService;
import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.content.MinimalChangeset;
import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.pull.PullRequest;
import com.atlassian.stash.pull.PullRequestSearchRequest;
import com.atlassian.stash.pull.PullRequestService;
import com.atlassian.stash.pull.PullRequestState;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.util.Page;
import com.atlassian.stash.util.PageRequest;
import com.atlassian.stash.util.PageRequestImpl;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

/**
 * @author Georgy Bolyuba (georgy@bolyuba.com)
 */
public class ChangesetFilter implements Filter {

    private final BuildStatusService buildStatusService;
    private final PullRequestService pullRequestService;
    private final AutomergeService automergeService;


    public ChangesetFilter(BuildStatusService buildStatusService, PullRequestService pullRequestService, NavBuilder navBuilder, AutomergeService automergeService) {
        this.buildStatusService = buildStatusService;
        this.pullRequestService = pullRequestService;
        this.automergeService = automergeService;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // nothing
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        // let the Build API do it's work
        filterChain.doFilter(servletRequest, servletResponse);

        if (!HttpServletRequest.class.isInstance(servletRequest)) {
            // yeah, right
            return;
        }

        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        if (!httpServletRequest.getMethod().equalsIgnoreCase("POST")) {
            // Olny care about updates, ignore reads
            return;
        }

        String requestURI = httpServletRequest.getRequestURI();
        String sha1 = requestURI.substring(requestURI.lastIndexOf('/') + 1);
        replicateStatusIfNeeded(sha1);
    }

    private void replicateStatusIfNeeded(String updatedSHA1) {
        BuildStatus lastStatus = automergeService.findLastStatus(updatedSHA1);
        if (lastStatus == null) {
            //nothing to replicate...
            return;
        }

        // Iterate over all open pull requests
        PageRequest pageRequest = new PageRequestImpl(0, 1000);
        final PullRequestSearchRequest searchRequest = new PullRequestSearchRequest.Builder().state(PullRequestState.OPEN).build();
        Page<PullRequest> search = pullRequestService.search(searchRequest, pageRequest);

        // First batch
        Iterator<PullRequest> iterator = search.getValues().iterator();
        while (iterator.hasNext()) {
            PullRequest pullRequest = iterator.next();
            boolean shouldStop = replicateAndReport(pullRequest, updatedSHA1, lastStatus);
            if (shouldStop) {
                return;
            }
        }

        // Can I haz moar?
        while (!search.getIsLastPage()) {
            search = pullRequestService.search(searchRequest, search.getNextPageRequest());

            iterator = search.getValues().iterator();
            while (iterator.hasNext()) {
                PullRequest pullRequest = iterator.next();
                boolean shouldStop = replicateAndReport(pullRequest, updatedSHA1, lastStatus);
                if (shouldStop) {
                    return;
                }
            }
        }
    }

    private boolean replicateAndReport(PullRequest pullRequest, String sha1, BuildStatus buildStatus) {
        if (!automergeService.shouldReplicate(pullRequest.getToRef().getRepository().getId())) {
            // automerge build status replication is disabled for this repo, skip it
            return false;
        }

        Changeset automergeChangeset = automergeService.findAutomergeChangeset(pullRequest);
        if (automergeChangeset == null) {
            // probably no such branch, keep looking
            return false;
        }

        // check if report is for our changeset
        if (!automergeChangeset.getId().equalsIgnoreCase(sha1)) {
            // nope, keep looking
            return false;
        }

        String tipOfTheFromRefSha1 = pullRequest.getFromRef().getLatestChangeset();

        // One of the parents of automerge must be tip of from ref (that tip is what Build API looks at)
        for (MinimalChangeset parent : automergeChangeset.getParents()) {
            if (tipOfTheFromRefSha1.equalsIgnoreCase(parent.getId())) {
                // found it, replicate
                buildStatusService.add(tipOfTheFromRefSha1, new BuildStatusWrapper(buildStatus, sha1));
                return true;
            }
        }

        // can this even happen?
        return false;
    }

    @Override
    public void destroy() {
        // this.DieYouGravySuckingPigDog();
    }

    // Wrap BuildStatus to change description
    private static class BuildStatusWrapper implements BuildStatus {
        private BuildStatus target;
        private String sha1;

        private BuildStatusWrapper(@Nonnull BuildStatus target, @Nonnull String sha1) {
            this.target = target;
            this.sha1 = sha1;
        }

        @Nonnull
        @Override
        public BuildStatus.State getState () {
            return target.getState();
        }

        @Nonnull
        @Override
        public String getKey () {
            return target.getKey();
        }

        @Nullable
        @Override
        public String getName () {
            return "Automerge changeset [" + sha1 + "]";
        }

        @Nonnull
        @Override
        public String getUrl () {
            return target.getUrl();
        }

        @Nonnull
        @Override
        public Date getDateAdded () {
            return target.getDateAdded();
        }

        @Nullable
        @Override
        public String getDescription () {
            return "Note: This is a build status for a shadow automerge branch";
        }

        @Override
        public int hashCode() {
            return target.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            return target.equals(obj);
        }

        @Override
        public String toString() {
            return target.toString();
        }
    }
}
