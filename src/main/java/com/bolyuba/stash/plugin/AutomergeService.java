package com.bolyuba.stash.plugin;

import com.atlassian.stash.build.BuildStatus;
import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.pull.PullRequest;

import javax.annotation.Nonnull;

/**
 * @author Georgy Bolyuba (georgy@bolyuba.com)
 */
public interface AutomergeService {

    /**
     * @param pullRequest
     * @return Changeset of successful automerge if there is one, {@code null} otherwise
     */
    public Changeset findAutomergeChangeset(@Nonnull PullRequest pullRequest);

    /**
     *
     * @param sha1
     * @return
     */
    public BuildStatus findLastStatus(@Nonnull String sha1);

    /**
     * @param repoId a repository id
     * @return {@true} if automerge replication functionality is enabled for the repository, {@false} otherwise
     */
    public boolean shouldReplicate(@Nonnull Integer repoId);

    /**
     * Enables/Disables automerge build status replication for given repo
     *
     * @param repoId
     * @param enabled
     */
    public void setReplicate(@Nonnull Integer repoId, boolean enabled);

    public boolean shouldCheckMerge(@Nonnull Integer repoId);

    public void setCheckMerge(@Nonnull Integer repoId, boolean enabled);


}
