package com.bolyuba.stash.plugin.config.db;

import net.java.ao.Entity;
import net.java.ao.Preload;

/**
 * Flag showing if automerge build status replication is enabled for given repo. In a context of pull request
 * "given repo" is destination repository (naturally).
 *
 * @author <a href="mailto:georgy@bolyuba.com">Georgy Bolyuba</a>
 */
@Preload
public interface StatusSettings extends Entity {

    boolean getReplicate();

    void setReplicate(boolean enabled);

    boolean getMergeCheck();

    void setMergeCheck(boolean enabled);

    int getRepoId();

    void setRepoId(int repoId);

}
