package com.bolyuba.stash.plugin.config.ui;

import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.ui.ContextualFormFragment;
import com.atlassian.stash.ui.ValidationErrors;
import com.atlassian.stash.view.TemplateRenderingException;
import com.bolyuba.stash.plugin.AutomergeService;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

/**
 * @author <a href="mailto:georgy@bolyuba.com">Georgy Bolyuba</a>
 */
public class ReplicateBuildStatusConfig implements ContextualFormFragment {

    private final AutomergeService automergeService;

    private final SoyTemplateRenderer soyTemplateRenderer;

    private static final String AUTOMERGE_REPLICATE = "automergeReplicate";

    private static final String TEMPLATE_NAME = "stash.plugins.buildstatus.fragmentReplicate";


    public ReplicateBuildStatusConfig(AutomergeService automergeService, SoyTemplateRenderer soyTemplateRenderer) {
        this.automergeService = automergeService;
        this.soyTemplateRenderer = soyTemplateRenderer;
    }

    @Override
    public void doView(Appendable appendable, Map<String, Object> context) throws IOException {
        Repository repository = (Repository) context.get("repository");
        context.put(AUTOMERGE_REPLICATE, automergeService.shouldReplicate(repository.getId()));
        renderView(appendable, context);
    }

    @Override
    public void validate(Map<String, String[]> stringMap, ValidationErrors validationErrors, Map<String, Object> stringObjectMap) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void doError(Appendable appendable, Map<String, String[]> stringMap, Map<String, Collection<String>> stringCollectionMap, Map<String, Object> stringObjectMap) throws IOException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void execute(Map<String, String[]> requestParams, Map<String, Object> context) {
        Repository repository = (Repository) context.get("repository");

        automergeService.setReplicate(repository.getId(), isEnabled(requestParams.get(AUTOMERGE_REPLICATE)));
    }

    private void renderView(Appendable appendable, Map<String, Object> context) {
        try {
            soyTemplateRenderer.render(appendable, "com.bolyuba.stash.plugin.stash-build-status-plugin:stash-build-status-soy-templates",
                    TEMPLATE_NAME, context);
        } catch (SoyException e) {
            throw new TemplateRenderingException("Failed to render " + TEMPLATE_NAME, e);
        }
    }

    private boolean isEnabled(String[] values) {
        return values != null && values.length > 0;
    }
}
